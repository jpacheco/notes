# OpenBSD.

Hablemos de OpenBSD

Es un sistema seguro, minimal, y potente, el cual puede revivir praticamente 
cualquier computadora (con algunas excepciones). Esta mas enfocada a los servidores.
Aun asi se puede utilizar este OS como desktop, o como gestor de ventanas.

## Instalacion:

El proceso de instalacion de obds es relativamente sencillo:
Solamente hay que seguir los pasos que te da el prompt en el shell y es todo.

Pero si que hay unos pasos que son un poco *confuzos*.

1.- Las particiones:

    Aqui es basicamete darle next a todo, pero si es importante saber lo que el 
    instalador hara con tus particiones. En este caso sera seleccionar el disco 
    completo *whole*.

2.- los *FileSystem*:

    En el caso que yo recomiendo es el de utilizar la imagen: *miniroot.img*,
    ya que es la mas liviana, y al llegar a este punto habra que seleccionar 
    http, para poder descargar los filesystem desde algun servidor de obds,
    en este caso sera: cdn.openbsd.org
